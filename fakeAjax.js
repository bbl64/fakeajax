(function(globals) {

    /**
     * Fakes and ajax request, executing the callback function
     * after a random time interval
     * @param {string} url - URL for the request you want to get back
     * @param {function} cb - callback method to be executed when
     */
    globals.fakeAjax = function(url,cb) {
        var fake_responses = {
            'request1': 'The first text',
            'request2': 'The middle text',
            'request3': 'The last text'
        };
        var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

        console.log('Requesting: ' + url);

        setTimeout(function(){
            cb(fake_responses[url]);
        },randomDelay);
    };

    /**
     * Output a given string in the console
     * @param {string} text - string to be outputed in the console.
     */
    globals.output = function(text) {
        console.log(text);
    };

    /**
     * Generate a simple promise (by utilising es6 Promises)
     * @param requestString - the request input string
     * @returns {Promise} promise - the promise which can be used for the asynchronous call
     */
    globals.makeRequestPromise = function( requestString ) {
        return new Promise(function(resolve, reject) {
            this.fakeAjax( requestString, resolve );
        });
    }

    /**
     * Launch 3 requests in "parallel" and using Promise.all to wait for promises to finish.
     * A bit more handy when dealing with dynamic number of Promises.
     */
    globals.launchRequests = function() {
        var requests = ['request1', 'request2', 'request3'];

        var promises = requests.map(function( request ) {
            var promise = makeRequestPromise( request );
            return promise;
        });

        // Wait for all promises to finish
        Promise.all(promises).then(function( outputArray ) {

            if( outputArray ) {

                outputArray.forEach( function( outputString ) {
                    output( outputString)
                });
                output('Completed');

                document.body.innerHTML = '<h1>The awesome stuff in the background did now finish!</h1>';
            }
        });

    }

    /**
     * Launch 3 requests in "parallel" but executing them in a series.
     * Here the promises are chained one by one.
     */
    globals.chainingApproach = function() {
        var requests = ['request1', 'request2', 'request3'];

        var promises = requests.map(function( request ) {
            var promise = makeRequestPromise( request );
            return promise;
        });
        promises.push( Promise.resolve(1));

        var promiseSeries = function(list) {
            var p = Promise.resolve();
            return list.reduce(function(pacc, fn, currentIndex) {
                return pacc = pacc.then( function( data ) {
                    if( data )output(data);

                    if( currentIndex === list.length-1 ) output( 'Completed! ');
                    else return fn;

                });
            }, p);
        };

        promiseSeries(promises);
    }

    /**
     * Initializes the request process
     */
    globals.init = function() {
        document.body = document.createElement("body");
        document.body.innerHTML = '<h1>Something awesome might be happening in the background!</h1>';

        this.launchRequests();
    }



})(window);